var categories = function () {
    // Targets page element
    var cat_insert = document.getElementById('cui-paper');

    // Get JSON with tags
    function get_cat_json() {
        var res_json = [{}];
        $.ajax(
            {
                type: 'GET',
                headers: {Authorization: 'Basic ' + btoa(logpass)},
                url: "http://localhost:9998/time-planner/web/rest/tags",
                dataType: "json",
                async: false,
                success: function (tasks_json) {
                    res_json = tasks_json;
                }
            }
        );
        return res_json
    }

    // Put JSON into variable
    var data_json = get_cat_json();

    // Paste to page
    for (var i = 0; i < data_json.length; i++) {
        var main_div = document.createElement('div');
        main_div.className = 'param-specs param-spec-property';

        var div_psp = document.createElement('div');
        div_psp.className = 'param-spec-property';
        main_div.appendChild(div_psp);

        var div_hc = document.createElement('div');
        div_hc.className = 'header-column';
        div_hc.innerText = data_json[i].id;
        div_hc.style.fontWeight = 'bolder';
        div_psp.appendChild(div_hc);

        var div_cce = document.createElement('div');
        div_cce.className = 'control-column-expanded';
        div_psp.appendChild(div_cce);

        var div_cc = document.createElement('div');
        div_cc.className = 'context-container';
        div_cce.appendChild(div_cc);

        var span_expl = document.createElement('span');
        span_expl.className = 'context-label';
        span_expl.style.display = 'block';
        span_expl.innerHTML = data_json[i].name + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';

        let del_btn = document.createElement('button');
        del_btn.type = 'button';
        del_btn.className = 'btn btn-mini btn-link';
        del_btn.textContent = 'Удалить';
        del_btn.setAttribute('value', data_json[i].id);
        del_btn.setAttribute('name', data_json[i].name);

        span_expl.appendChild(del_btn);

        div_cc.appendChild(span_expl);

        cat_insert.appendChild(main_div)

    }

    $('.btn-link').each(function () {
        let id = $(this).attr('value');
        let name = $(this).attr('name');
        $(this).click( function () {
        $.ajax({
                type: 'DELETE',
                headers: {Authorization: 'Basic ' + btoa(logpass)},
                url: "http://localhost:9998/time-planner/web/rest/tasks",
                data: JSON.stringify({id: id, name: name}),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
            });
            window.location.replace('tags.html')
        });
    });

};
