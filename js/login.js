// Function provides authentification
$(document).ready(function() {
   $('#submit').click(function () {
       var cookie_name = 'logpass';
       var logpass= $('#username').val() + ':' + $('#password').val();
       document.cookie = cookie_name + '=' + logpass;

       $.ajax({
           type: 'GET',
           headers: {Authorization: 'Basic ' + btoa(logpass)},
           url: "http://localhost:9998/time-planner/web/rest/users/login"
       }).done(function () {
           window.location.replace("main.html?date=week");
       }).fail(function () {
           window.alert("Неверное имя пользователя или пароль!");
       });
   })
});


