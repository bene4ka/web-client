// Function provides category creation
$(document).ready(function() {
    $('#submit').click(function () {
        let categoryName = $('#categoryName').val();
        let categoryText = $('#categoryText').val();
        $.ajax({
            type: 'POST',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/categories",
            data: JSON.stringify({ name: categoryName , description: categoryText }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        })
    })
});
