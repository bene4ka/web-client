// Function provides task submitting
$(document).ready(function() {
    $('#submit').click(function () {
        let taskName = $('#taskName').val();
        let taskDesc = $('#taskDesc').val();
        let category_id = $('#category').val();

        function getCatJson() {
            let category_json = {};
            $.ajax(
                {
                    type: 'GET',
                    headers: {Authorization: 'Basic ' + btoa(logpass)},
                    url: "http://localhost:9998/time-planner/web/rest/categories/" + category_id,
                    dataType: "json",
                    async: false,
                    success: function (category_result) {
                        category_json = category_result;
                    }
                }
            );
            return category_json
        }


        let priority = $("input[name='reviewStars']:checked").val();
        let startDate = $('#startDate').val();
        let dueDate = $('#dueDate').val();
        let tags = $('#tags').val();
        let tags_array = tags.split(',');

        if (tags_array.length === 0) {tags_array = []}

        function makeTagAndReturnIt() {
            let result = [];
            // Create tags
            for (let i = 0 ; i < tags_array.length ; i++ ) {
                $.ajax({
                    type: 'POST',
                    headers: {Authorization: 'Basic ' + btoa(logpass)},
                    url: "http://localhost:9998/time-planner/web/rest/tags",
                    data: JSON.stringify({ name: tags_array[i] }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                })
            }

            let all_tags = [];
            function getTags() {
                $.ajax(
                    {
                        type: 'GET',
                        headers: {Authorization: 'Basic ' + btoa(logpass)},
                        url: "http://localhost:9998/time-planner/web/rest/tags",
                        dataType: "json",
                        async: false,
                        success: function (tasks_result) {
                            all_tags = tasks_result;
                        }
                    }
                );
                return all_tags;
            }
            getTags();


            for (let i = 0 ; i < all_tags.length; i++) {
                for (let j = 0 ; j < tags_array.length ; j++) {
                    if (all_tags[i]['name'] === tags_array[j]) {
                        result.push(all_tags[i])
                    }
                }
            }

            return result
        }


        let timeScale = {"id": 2, "scale": "month"};

        let tags_result = makeTagAndReturnIt();
        // console.log(tags_result);

        $.ajax({
            type: 'POST',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/tasks",
            data: JSON.stringify({ category: getCatJson() , description: taskDesc , dueDate: dueDate , priority: priority , startDate: startDate , tags: tags_result , title: taskName , timeScale: timeScale }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false
        });
        window.location.replace('main.html?date=week')
    })
});


