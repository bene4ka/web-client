// Function catches task id
function getTaskId(variable) {
    // Cut GET query part
    let query = window.location.search.substring(1);
    // Split by '=' as delimiter
    let pair = query.split('=');
    // Check if variable provided is query part
    // If so, return value provided
    // Else return 'weeks' because this is default value
    if (pair[0] === variable) {
        return pair[1];
    }
    else {
        return '1'
    }
}

function getTaskById(taskId) {
    let res_json = [{}];
    $.ajax(
        {
            type: 'GET',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/tasks/" + taskId,
            dataType: "json",
            async: false,
            success: function (tasks_json) {
                res_json = tasks_json;
            }
        }
    );
    return res_json
}

function fillForms(json) {
    let category_form = document.getElementById('category');
    let description_form = document.getElementById('taskDesc');
    let dueDate_form = document.getElementById('dueDate');
    let priority_form = document.getElementById("star-" + (json['priority'] - 1));
    let startDate_form = document.getElementById('startDate');
    let tags_form = document.getElementById('tags');
    let title_form = document.getElementById('taskName');
    title_form.value = json['title'];
    description_form.value = json['description'];
    category_form.value = json['category']['id'];
    priority_form.checked = true;
    startDate_form.value = json['startDate'];
    dueDate_form.value = json['dueDate'];
    let tags_string = '';
    for ( let i = 0 ; i < json['tags'].length ; i++) {
        let task_name = json['tags'][i]['name'];
        tags_string = tags_string + ',' + task_name
    }
    tags_string = tags_string.substring(1);
    tags_form.value = tags_string
}

function editTask(taskJSON) {

    let tags = $('#tags').val();

    let tags_array = tags.split(',');
    if (tags_array.length === 0) {tags_array = []}

    function makeTagAndReturnIt() {
        let result = [];
        // Create tags
        for (let i = 0 ; i < tags_array.length ; i++ ) {
            $.ajax({
                type: 'POST',
                headers: {Authorization: 'Basic ' + btoa(logpass)},
                url: "http://localhost:9998/time-planner/web/rest/tags",
                data: JSON.stringify({ name: tags_array[i] }),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
            })
        }

        let all_tags = [];
        function getTags() {
            $.ajax(
                {
                    type: 'GET',
                    headers: {Authorization: 'Basic ' + btoa(logpass)},
                    url: "http://localhost:9998/time-planner/web/rest/tags",
                    dataType: "json",
                    async: false,
                    success: function (tasks_result) {
                        all_tags = tasks_result;
                    }
                }
            );
            return all_tags;
        }
        getTags();


        for (let i = 0 ; i < all_tags.length; i++) {
            for (let j = 0 ; j < tags_array.length ; j++) {
                if (all_tags[i]['name'] === tags_array[j]) {
                    result.push(all_tags[i])
                }
            }
        }

        return result
    }

    let taskForDeletionId = getTaskId('id');

    $('#edit').click(function () {
        let taskName = $('#taskName').val();
        let taskDesc = $('#taskDesc').val();
        let category_id = $('#category').val();

        function getCatJson() {
            let category_json = {};
            $.ajax(
                {
                    type: 'GET',
                    headers: {Authorization: 'Basic ' + btoa(logpass)},
                    url: "http://localhost:9998/time-planner/web/rest/categories/" + category_id,
                    dataType: "json",
                    async: false,
                    success: function (category_result) {
                        category_json = category_result;
                    }
                }
            );
            return category_json
        }

        let priority = $("input[name='reviewStars']:checked").val();
        let startDate = $('#startDate').val();
        let dueDate = $('#dueDate').val();

        $.ajax({
            type: 'DELETE',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/tasks",
            data: JSON.stringify({id: taskForDeletionId}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        });

        let timeScale = {"id": 2, "scale": "month"};
        let tags_result = makeTagAndReturnIt();
        // console.log(tags_result);

        $.ajax({
            type: 'POST',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/tasks",
            data: JSON.stringify({ category: getCatJson() , description: taskDesc , dueDate: dueDate , priority: priority , startDate: startDate , tags: tags_result , title: taskName, timeScale: timeScale }),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false
        });
        setTimeout(window.location.replace('main.html?date=week'),10000)
    });

}

function delTask(taskJSON) {
    $('#delete').click(function () {
        let taskForDeletionId = getTaskId('id');

        $.ajax({
            type: 'DELETE',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/tasks",
            data: JSON.stringify({id: taskForDeletionId}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false
        });
        setTimeout(window.location.replace('main.html?date=week'),10000)
    });

}