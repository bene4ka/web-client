// Receives tasks and returns for usage on page
function data_json() {
    var res_json = [{}];
    $.ajax(
        {
            type: 'GET',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/tasks",
            dataType: "json",
            async: false,
            success: function (tasks_json) {
                res_json = tasks_json;
            }
        }
    );
    return res_json
}

function metadata_json() {
    let metares_json = [{}];
    $.ajax(
        {
            type: 'GET',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/metaTasks",
            dataType: "json",
            async: false,
            success: function (tasks_json) {
                metares_json = tasks_json;
            }
        }
    );
    return metares_json
}

function one_metatask(id) {
    let metatask_json = [{}];
    $.ajax(
        {
            type: 'GET',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/metaTasks/" + id,
            dataType: "json",
            async: false,
            success: function (tasks_json) {
                metatask_json = tasks_json;
            }
        }
    );
    return metatask_json
}

// Function catches date scale - week, month or year
function getQueryVariable(variable) {
    // Cut GET query part
    let query = window.location.search.substring(1);
    // Split by '=' as delimiter
    let pair = query.split('=');
    // Check if variable provided is query part
    // If so, return value provided
    // Else return 'weeks' because this is default value
    if (pair[0] === variable) {
        return pair[1];
    }
    else {
        return 'weeks'
    }
}

// Function counts days in this month
function daysInThisMonth(date) {
    let now = new Date(date);
    return new Date(now.getFullYear(), now.getMonth() + 1, 0).getDate();
}

// Fuction gets monday for provided day of the week
function GetMonday(provided_day) {
    provided_day = new Date(provided_day);
    let day = provided_day.getDay(),
        diff = provided_day.getDate() - day + (day === 0 ? -6:1); // adjust when day is sunday
    return new Date(provided_day.setDate(diff));
}

// Function gets sunday for provided day of the week
function GetSunday(provided_day) {
    provided_day = new Date(provided_day);
    let day = provided_day.getDay(),
        diff = provided_day.getDate() - (day - 1) + 6; // adjust when day is sunday
    return new Date(provided_day.setDate(diff));
}

// Function gets x/y for grid-column
function timeDiff(startDate, dueDate, range, date) {
    // Date mock
    let mock_date = new Date(date);
    // Correcting Sunday
    function correctSunday(date) {
        date = date.getDay();
        if (date === 0) {
            return 7
        }
        else {
            return date
        }
    }
    // Get Mon and Sun
    let week_start = new GetMonday(mock_date);
    let week_end = new GetSunday(mock_date);
    // Convert startDate and dueDate to data variable type
    let taskStarts = new Date(startDate);
    let taskEnds = new Date(dueDate);
    // By default we have no range for this task
    let result = 'NaN/NaN';
    // Function here will correct day range, because if we get task which have higher range than 1 week - this is
    // a task for all week length. JS also returns days like 1-6,0, not 1-7 or 0-6, so this also need to be fixed
    // in this function.
    if (range === 'week') {
        if (week_start > taskStarts) { taskStarts = week_start }
        if (week_end < taskEnds) { taskEnds = week_end }
        result = correctSunday(taskStarts) + '/' + (correctSunday(taskEnds) + 1);
    }
    // If range is month we just pass the days as digit to constructor
    else if (range === 'month') {
        result = taskStarts.getDate() + '/' + taskEnds.getDate()
    }
    // If year, than check month where task will be actual
    else {
        console.log(taskStarts.getMonth());
        console.log(taskEnds.getMonth());
        result = (taskStarts.getMonth() + 1) + '/' + (taskEnds.getMonth() + 1)
    }
    // If result is nothing (for some reason test data have tasks where JSON can provide no data for start and end!)
    // return 0. Else return two digits for grid-column tag.
    console.log(result);
    return result;
}

// JS html constructor
function BuildGantt (dynatasks, metatasks, range, date) {

    // Function rebuilds JSON - throws away tasks which we shouldn't see
    function rebuildTasks(dynatasks, metatasks, range) {

        // Cut metatasks from dynatasks
        // for (let i = 0 ; i < metatasks.length; i++) {
        //     for (let j = 0 ; j < metatasks.length; j++) {
        //         if (dynatasks[j]['id'] === dynatasks[i]['id']) {
        //             dynatasks.splice(j, 1);
        //         }
        //     }
        // }
        if (metatasks.length > 0) {
            // Cut subtasks from dynatasks
            for (let i = 0; i < metatasks.length; i++) {
                let subtasks = metatasks[i]['subTasks'];
                for (let j = 0; j < subtasks.length; j++) {
                    for (let k = 0; k < dynatasks.length; k++) {
                        if (dynatasks[k]['id'] === subtasks[j]['id']) {
                            dynatasks.splice(k, 1);
                        }
                    }
                }
            }


            dynatasks = dynatasks.concat(metatasks);

        }

        // Today is a mock day (for future usage where we will forward and back weeks, months etc)
        let mock_date = date;
        // Two wariables tells us where grid starts and ends
        if (range === 'week') {
            var gridstart = GetMonday(mock_date);
            var gridend = GetSunday(mock_date);
        }
        else if (range === 'month') {
            gridstart = new Date(mock_date.getFullYear(), mock_date.getMonth(), 1);
            gridend = new Date(mock_date.getFullYear(), mock_date.getMonth() + 1, 0);
        }
        else {
            gridstart = new Date(mock_date.getFullYear(), 0, 1);
            gridend = new Date(mock_date.getFullYear(), 12, 1);
        }
        // Here we will put tasks which is matching by dates for adding to grid
        let rebuildedTasks = [];
        // For each task get start and end and check if dates is in our time range
        for (let i = 0; i < dynatasks.length; i++) {

            let startDate = new Date(dynatasks[i]["startDate"]);
            let dueDate = new Date(dynatasks[i]["dueDate"]);
            if (dueDate < gridstart) {

            }
            else if (startDate > gridend) {

            }
            else {
                rebuildedTasks.push(dynatasks[i]);
            }
            }
        // This part of code sorting array of associated arrays by 'startDate' key
        // Tasks which is starting earlier goes higher in array
        rebuildedTasks.sort(function(a,b) {
            let bdate = new Date(b.startDate);
            let adate = new Date(a.startDate);
            return adate - bdate
        });
        return rebuildedTasks
        }
    let tasks = rebuildTasks(dynatasks, metatasks, range);
    // Style for li tag represents gant rows
    function li_style(priority) {
        if ( priority === 1 ) {var style = "; background-color: #2ecaac;"}
        else if ( priority === 2 ) {style =  "; background-color: #2ecaac;"}
        else if ( priority === 3 ) {style =  "; background-color: #54c6f9;"}
        else if ( priority === 4 ) {style = "; background-color: #54c6f9;"}
        else { style = "; background-color: #ff6252;"}
        return style
    }
    // Function makes gant rows much beautier
    function li_class(priority) {
        if ((priority === 2) || (priority === 4) || priority === 5) {
            var style_class = 'stripes'
        }
        else style_class = '';
        return style_class
    }
    // Array with day names
    const day_names =
        [
            'Понедельник',
            'Вторник',
            'Среда',
            'Четверг',
            'Пятница',
            'Суббота',
            'Воскресенье'
        ];
    // Array with month names
    const month_names =
        [
            'Январь',
            'Февраль',
            'Март',
            'Апрель',
            'Май',
            'Июнь',
            'Июль',
            'Август',
            'Сентябрь',
            'Октябрь',
            'Ноябрь',
            'Декабрь'
        ];
    // Array with digits from first to last day of the month
    let days_high_value = daysInThisMonth(date) + 1;
    let days_counter = [];
    for (let day_in_month = 1; day_in_month < days_high_value; day_in_month++ ) {
     days_counter.push(day_in_month)
    }
    // Depending on choosen date range we put in variable day names OR month's day digits OR month names
    var grid_range = [];
    if (range === 'week') {
        grid_range = day_names
    }
    else if (range === 'month') {
        grid_range = days_counter;
    }
    else if (range === 'year') {
        grid_range = month_names
    }
    // Search first tag with gantt class
    let gantt_tag = document.getElementsByClassName("gantt")[0];
        // And making here div for time range
        let time_range_div = document.createElement('div');
        time_range_div.className = 'gantt--row gantt--row--' + range + 's';
        gantt_tag.appendChild(time_range_div);
            // Start to fill
            let header = document.getElementsByClassName('gantt--row--' + range + 's')[0];
            let empty_span = document.createElement('span');
            header.appendChild(empty_span);
            // As much times as many entries in grid_range
            for (let header_counter = 0; header_counter < grid_range.length; header_counter++) {
                let range_span = document.createElement("span");
                range_span.innerHTML = grid_range[header_counter];
                header.appendChild(range_span)
            }
        // Create div for vertical lines
        let lines_div = document.createElement('div');
        lines_div.className = 'gantt--row gantt--row--lines gantt--row--lines-repeat-' + range + 's';
        gantt_tag.appendChild(lines_div);
            // Fill it 
            let lines = document.getElementsByClassName("gantt--row--lines")[0];
            for (let col_counter = 0; col_counter < grid_range.length + 1 ; col_counter++) {
                let empty_span = document.createElement('span');
                empty_span.innerHTML = '';
                empty_span.className = 'row';
                lines.appendChild(empty_span)
            }
        // Start to fill grid from JSON
        for (let task = 0; task < tasks.length; task++) {
            let main_task_div = document.createElement('div');
            main_task_div.className = "gantt--row";
                // Outer div for line whith title name in it
                let outer_div = document.createElement('div');
                outer_div.className = "gantt--row-first";
                outer_div.innerHTML = tasks[task].title;
                main_task_div.appendChild(outer_div);
                // Than we add ul tag which makes style for line
                let style_ul = document.createElement('ul');
                style_ul.className = 'gantt--row-bars gantt--row-bars-rep-' + range + 's';
                style_ul.setAttribute('value', tasks[task].id);
                    // And in ul will create div for bar style
                    let dateRange = timeDiff(tasks[task]["startDate"], tasks[task]["dueDate"], range, date);
                    if (dateRange === '0')
                        {main_task_div.className = "gantt--row gantt--row--empty"}
                    else {
                    let li_tag = document.createElement('li');
                    let li_priority = tasks[task]["priority"];
                    li_tag.style = "grid-column: " + dateRange + li_style(li_priority);
                    li_tag.className = li_class(li_priority);
                    li_tag.innerHTML = tasks[task].description;
                    style_ul.appendChild(li_tag);}
                main_task_div.appendChild(style_ul);
            gantt_tag.appendChild(main_task_div);
        }
    // All columns for gantt chart
    let columns = document.getElementsByClassName('row');
    // The first and last columns where chart starts and ends
    let startrow = columns[1];
    let endrow = columns[ columns.length - 1 ];
    // Onclick date makes step back
    startrow.onclick = function myFunction() {
        date.setDate(date.getDate() - grid_range.length);
        ganttChart(dynatasks, metatasks, range, date);
    };
    // Onclick date makes step forward
    endrow.onclick = function myFunction() {
        date.setDate(date.getDate() + grid_range.length);
        ganttChart(dynatasks, metatasks, range, date);
    };


        let bars = document.getElementsByClassName('gantt--row-bars');

        for (let i = 0; i < bars.length; i++) {

            bars[i].addEventListener('click', clicks);
            let task_id = bars[i].getAttribute('value');

            // Global Scope variable we need this
            let clickCount = 0;
            // Our Timeout, modify it if you need
            let timeout = 500;

            function clicks() {
                // We modify clickCount variable here to check how many clicks there was

                clickCount++;
                if (clickCount === 1) {
                    setTimeout(function(){
                        if(clickCount === 1) {
                            window.location.replace("edittask.html?id=" + task_id)
                            // Single click invoke a task edit
                        } else {

                            // dynatasks = metatasks['subTasks'];
                            // console.log(dynatasks);
                            // console.log(metatasks);


                            for (i = 0; i < metatasks.length; i++) {
                                // console.log('iteration ' + i);
                                // console.log(metatasks[i]['id'] + ' is ' + task_id);
                                // console.log(metatasks[i]['id'] === Number(task_id));
                                // console.log(typeof metatasks[i]['id']);
                                // console.log(typeof task_id + '\n');
                                if (metatasks[i]['id'] === Number(task_id)) {
                                    dynatasks = metatasks[i]['subTasks']
                                }
                            }
                            metatasks = [];

                            // console.log(dynatasks);
                            // console.log(metatasks);

                            ganttChart(dynatasks, metatasks, range, date)

                            // Double invoke a function of viewing subtasks
                        }
                        clickCount = 0;
                    }, timeout || 300);
                }
            }
        }
}


function ganttChart(dynatask, metatasks, range, date) {
    let ganntBuilded = document.getElementsByClassName('gantt')[0];
    ganntBuilded.innerHTML = '';
    let gannt = new BuildGantt(dynatask, metatasks, range, date);
}
