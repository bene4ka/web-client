// Function gets cookie with username and pass
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function delCookie(cookie_name) {
    let logpass = "";
    document.cookie = cookie_name + '=' + logpass;
    return true
}

// Variable with username and pass
var logpass =  getCookie('logpass');

// If there is no cookie, redirect user to login page
function login_json() {
    $.ajax({
        type: 'GET',
        headers: {Authorization: 'Basic ' + btoa(logpass)},
        url: "http://localhost:9998/time-planner/web/rest/users/login",
        dataType: 'json',
        success: function (json_data) {
            var username_id = document.getElementById('username');
            username_id.innerHTML = json_data.firstName + ' ' + json_data.lastName
        }
    }).done(function (json) {
    }).fail(function () {
        window.location.replace("login.html");
    });
}



