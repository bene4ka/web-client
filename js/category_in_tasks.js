let categories = function () {
    // Targets page element
    var cat_dropdown = document.getElementsByClassName('custom-select')[0];

    // Get JSON for categories
    function get_cat_json() {
        var res_json = [{}];
        $.ajax(
            {
                type: 'GET',
                headers: {Authorization: 'Basic ' + btoa(logpass)},
                url: "http://localhost:9998/time-planner/web/rest/categories",
                dataType: "json",
                async: false,
                success: function (tasks_json) {
                    res_json = tasks_json;
                }
            }
        );
        return res_json
    }

    // Get JSON with function
    var data_json = get_cat_json();

    // Paste it to dropdown
    for (var i = 0; i < data_json.length; i++) {
        var menu_element = document.createElement('option');
        menu_element.innerText = data_json[i].name;
        menu_element.value = data_json[i].id;
        cat_dropdown.appendChild(menu_element)
    }
};