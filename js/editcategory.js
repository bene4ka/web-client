// Function catches task id
function getCategoryId(variable) {
    // Cut GET query part
    let query = window.location.search.substring(1);
    // Split by '=' as delimiter
    let pair = query.split('=');
    // Check if variable provided is query part
    // If so, return value provided
    // Else return 'weeks' because this is default value
    if (pair[0] === variable) {
        return pair[1];
    }
    else {
        return '1'
    }
}

function getCategoryById(taskId) {
    let res_json = [{}];
    $.ajax(
        {
            type: 'GET',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/categories/" + taskId,
            dataType: "json",
            async: false,
            success: function (tasks_json) {
                res_json = tasks_json;
            }
        }
    );
    return res_json
}

function fillForms(json) {
    let category_form = document.getElementById('categoryName');
    let description_form = document.getElementById('categoryText');

    description_form.value = json['description'];
    category_form.value = json['name'];
}

function editCategory(categoryJSON) {
    $('#edit').click(function () {
        let categoryName = $('#categoryName').val();
        let categoryDesc = $('#categoryText').val();
        let taskDeletionId = getCategoryId('id');

        $.ajax({
            type: 'DELETE',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/categories",
            data: JSON.stringify({id: taskDeletionId, name: categoryName, description: categoryDesc}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        });
        $.ajax({
            type: 'POST',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/categories",
            data: JSON.stringify({name: categoryName, description: categoryDesc}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        });
        window.location.replace('main.html?date=week')
    });
}

function delCategory(categoryJSON) {
    $('#delete').click(function () {
        let categoryName = $('#categoryName').val();
        let categoryDesc = $('#categoryText').val();
        let taskDeletionId = getCategoryId('id');

        $.ajax({
            type: 'DELETE',
            headers: {Authorization: 'Basic ' + btoa(logpass)},
            url: "http://localhost:9998/time-planner/web/rest/categories",
            data: JSON.stringify({id: taskDeletionId, name: categoryName, description: categoryDesc}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
        });
        window.location.replace('main.html?date=week')
    });

}