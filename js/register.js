// Function provides category creation
$(document).ready(function() {
    $('#submit').click(function () {
        let login = $('#inputLogin').val();
        let userName = $('#inputName').val();
        let userSName = $('#inputSname').val();
        let inputEmail = $('#inputEmail').val();
        let inputPassword = $('#inputPassword').val();

        $.ajax({
            type: 'POST',
            url: "http://localhost:9998/time-planner/web/rest/users",
            data: JSON.stringify({ email: inputEmail , firstName: userName , lastName: userSName , login: login , password: inputPassword}),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            async: false
        }).done(setTimeout(window.location.replace("login.html")), 500)
    })
});