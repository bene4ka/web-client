// Function for clocks in right upper panel
function startTime()
{
    var days = ['Понедельник','Вторник','Среда','Четверг','Пятница','Суббота','Воскресенье'];
    var months = ['Января','Февраля','Марта','Апреля','Мая','Июня','Июля','Августа','Сентября','Октября','Ноября','Декабря'];

    var tm=new Date();
    var dname = days[ tm.getDay() ];
    var mname = months[ tm.getMonth() ];
    var d=tm.getDate();
    var h=tm.getHours();
    var m=tm.getMinutes();
    var s=tm.getSeconds();
    m=checkTime(m);
    s=checkTime(s);
    document.getElementById('clock').innerHTML=h+":"+m+":"+s+" "+d+" "+mname+", "+dname;
    t=setTimeout('startTime()',500);
}
function checkTime(i)
{
    if (i<10)
    {
        i="0" + i;
    }
    return i;
}