var categories = function () {
    // Targets page element
    var cat_insert = document.getElementById('cui-paper');

    // Get JSON with categories
    function get_cat_json() {
        var res_json = [{}];
        $.ajax(
            {
                type: 'GET',
                headers: {Authorization: 'Basic ' + btoa(logpass)},
                url: "http://localhost:9998/time-planner/web/rest/categories",
                dataType: "json",
                async: false,
                success: function (tasks_json) {
                    res_json = tasks_json;
                }
            }
        );
        return res_json
    }

    // Get JSON with function
    var data_json = get_cat_json();

    // Paste it
    for (var i = 0; i < data_json.length; i++) {
        var main_div = document.createElement('div');
        main_div.className = 'param-specs param-spec-property';
            var div_psp = document.createElement('div');
            div_psp.className = 'param-spec-property';
            main_div.appendChild(div_psp);
                var div_hc = document.createElement('div');
                div_hc.className = 'header-column';
                div_hc.innerHTML = "<a href='editcategory.html?id=" + data_json[i].id + "'>" + data_json[i].name;
                div_hc.style.fontWeight = 'bolder';
                div_psp.appendChild(div_hc);
                var div_cce = document.createElement('div');
                div_cce.className = 'control-column-expanded';
                div_psp.appendChild(div_cce);
                    var div_cc = document.createElement('div');
                    div_cc.className = 'context-container';
                    div_cce.appendChild(div_cc);
                        var span_expl = document.createElement('span');
                        span_expl.className = 'context-label';
                        span_expl.style.display = 'block';
                        span_expl.innerText = data_json[i].description;
                        div_cc.appendChild(span_expl);
        cat_insert.appendChild(main_div)
    }
};

